import axios from 'axios'
import { MessageBox, Message } from 'element-ui'

// create an axios instance
const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API, // url = base url + request url
  // withCredentials: true, // send cookies when cross-domain requests
  timeout: 30000 // request timeout
})

// request interceptor
service.interceptors.request.use(
  config => {
    /**
     * 统一处理请求头。加上 token, language 等属性
     */
    config.headers['X-Token'] = 'xxx';
    return config
  },
  err => {
    console.log('request: ' + err); // for debug
    return Promise.reject(err)
  }
)

// response interceptor
service.interceptors.response.use(
  res => {
    /**
     * 请求超时或其它没有得到后台数据时，需要判断是否重试。
     * retry 和 count 属性分别记录该请求重试的次数的上限和已经请求的次数。
     * 如果次数没有达到上限，则次数累加1，并重发该请求，返回一个新的Promise。
     */
    const { headers } = res.config;
    if (!res.data && headers.retry) {
      if (headers.count < headers.retry) {
        headers.count = ~~headers.count + 1;
        return service(res.config)
      }
    }

    /**
     * code 数字的对应关系
     * 2000 = success
     * 5008: Illegal token
     * 5012: Other clients logged in
     * 5014: Token expired
     */
    const { code, message = 'Error' } = res.data || {};
    if (code !== 20000) {
      Message({
        message: message,
        type: 'error',
        duration: 5000
      })
      if (code === 50008 || code === 50012 || code === 50014) {
        // 重定向到登陆页
      }
      return Promise.reject(new Error(message))
    }

    return res
  },
  err => {
    console.log('response: ' + err); // for debug
    Message({
      message: err.message,
      type: 'error',
      duration: 5 * 1000
    })
    return Promise.reject(err)
  }
)

export default service
