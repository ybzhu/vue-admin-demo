import Vue from "vue";

import 'normalize.css/normalize.css' // a modern alternative to CSS resets
import Element from 'element-ui'

import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";

Vue.use(Element);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
